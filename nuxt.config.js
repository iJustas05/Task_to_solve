export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,

  server: {
    port: 3004, // default: 3000
    host: 'localhost', // default: localhost,
    timing: true,
  },
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'testukas',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [{src:'~/assets/app.scss', lang: 'scss'}],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [{src:'~/plugins/bootstrap.js', mode: 'client'},
    {src: '~/plugins/vue2-filters'}],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
  ],
  router: {
    middleware: ['auth']
  },
  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
      '@nuxtjs/auth-next'
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
axios: {
  /**
    When issuing a request to baseURL that needs to pass authentication headers to
    the backend, 'credentials' should be set to 'true'
  */
  credentials: true, // default value of withCredentials is fale
  proxy: true,
  // This is where to hit the server
  debug: false,
  baseUrl: 'http://localhost:8001',
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*'
  }
},
  proxy: {
    '/laravel/': {
      target: 'http://localhost:8001',
      pathRewrite: { "^/laravel/": "" }
    },
  },
  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  },

  auth: {
   redirect: {
     login: '/login',
     logout: '/',
     register: '/register',
     callback: '/login',
     home: '/'
   },
   strategies: {
     laravelSanctum: {
        provider: 'laravel/sanctum',
        url: 'http://localhost:3004/laravel/',
        endpoints: {
          login: { url: 'api/login', method: 'post' },
          register: { url: 'api/register', method: 'post' },
          logout: { url: 'api/logout', method: 'post'}
        },
        tokenRequired: false,
        tokenType: false
      }
  },
  localStorage: false
},

}
